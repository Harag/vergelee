(in-package :vergelee)

(defun get-token (server-url credentials)
  "Returns a token if credentials passde muster."
  (drakma:http-request
   (format nil "http://~A:4321/token" (or server-url "localhost"))
   :method :post
   :parameters (list (cons "credentials" (format nil "~S" credentials)))))

(defun remote-eval (server-url sexp token)
  "Returns result of sexp evaluation."
  (drakma:http-request
   (format nil "http://~A:4321/eval?token=~A" (or server-url "localhost") token)
   :method :post
   :parameters (list (cons "sexp" (format nil "~S" sexp)))))
