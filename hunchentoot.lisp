(in-package :vergelee)

(defmacro with-debugging (&body body)
  ;; Using this as debugging tool because hunchentoot
  ;; swallows all errors here if set to swallow errors.
  `(handler-bind ((error #'invoke-debugger))
     ,@body))

(defclass hunchentoot-server (server hunchentoot:easy-acceptor)
  ())


(defmethod start-server ((server hunchentoot-server) &key &allow-other-keys)
  (hunchentoot:start server))

(defmethod bad-request ((request hunchentoot:request) &key &allow-other-keys)
  (setf (hunchentoot:return-code hunchentoot:*reply*)
	hunchentoot:+http-bad-request+)
  (hunchentoot:abort-request-handler))


(defmethod hunchentoot:handle-request ((acceptor hunchentoot-server) request)  
  (with-debugging
  ;;  (break "~A~%~S"(hunchentoot:post-parameters*) request)
    (cond ((equalp (hunchentoot:script-name request) "/eval")	     
	   (handle-eval acceptor
			request
			(or (hunchentoot:get-parameter "sexp")
			    (hunchentoot:post-parameter "sexp"))
			:credentials (list :credentials (hunchentoot:post-parameter "credentials"))
			:token (hunchentoot:get-parameter "token")))
	  ((equalp (hunchentoot:script-name request) "/token")
           
	   (handle-token acceptor request (hunchentoot:post-parameter "credentials"))))))



