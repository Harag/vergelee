(in-package :common-lisp-user)

(defpackage :vergelee
  (:use :cl)
  (:export

   ;;Server
   :server
   :url
   :port
   :sessions

   :authorization

   :session
   :cache

   :authenticate
   :issue-token
   :start-session
   :start-server
   :handle-eval
   :vergelee-eval

   ;;Client
   :get-token
   :remote-eval
  
   ))
