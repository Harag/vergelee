(in-package :vergelee)

(defclass server ()
  ((url :initarg :url
	:initform "localhost"
	:accessor url
	:documentation "The external url to the server. Using url instead of breaking things up into host etc might have to revisit this have to look at how different http servers work..")
   (port :initarg :port
	 :initform 5000
	 :accessor port
	 :documentation "Port used by server.")
   (sessions :initarg :sessions
	     :initform (make-hash-table :test 'equal)
	     :accessor sessions))
  (:documentation "Used to set some basic info needed by http server, and the apis.
Specialize this class for different http servers backends."))

(defclass authorization ()
  ((credentials :initarg :credentials
		:initform nil
		:accessor credentials
		:documentation "A plist representing the credentials of then caller.")
   (token :initarg :token
	  :initform nil

	  :accessor token
	  :documentation "A token issued to the caller for the session. The token can be used to do remote evals.")))

(defclass session ()
  ((server :initarg :server
	       :initform nil
	       :accessor server
	       :documentation "Reference to server for convenience.")
   (authorization :initarg :authorization
		  :initform nil
		  :accessor authorization
		  :documentation "An object that represents the authorization of the api caller.")
   (cache :initarg :cache
	  :initform (make-hash-table :test 'equal)
	  :accessor cache
	  :documentation "A place to cache session values. Doing caching on session level, so that memory can be release when the session dies."))
  (:documentation "To do remote evals the caller must first establish a session, with the correct authentication credentials."))

(defgeneric authenticate (credentials &key &allow-other-keys)
  (:documentation "Checks credentials of caller."))

(defmethod authenticate (credentials &key &allow-other-keys)
  (when credentials
    t))

(defgeneric issue-token (server &key credentials)
  (:documentation "If credentials pass issue a token for use with eval."))

(defmethod issue-token (server &key credentials)
  (declare (ignorable credentials))
  (let ((key (ironclad:random-data 8)))
    (ironclad:byte-array-to-hex-string
     (ironclad:produce-mac (ironclad:make-blake2-mac key)))))

(defgeneric start-session (server credentials &key token &allow-other-keys)
  (:documentation "Creates a session and issues a token. If session for token already exists returns existing session."))

(defmethod start-session (server credentials &key token &allow-other-keys)
  (let ((token (or token (if (authenticate credentials)
			     (issue-token credentials))))
	(session (gethash token (sessions server))))
    (when (and token (not session))
	  (setf session (make-instance 'session
				       :server server
				       :authorization
				       (make-instance 'authorization
						      :credentials credentials
						      :token token)))
	  (setf (gethash token (sessions server)) session))
	session))

(defgeneric start-server (server &key &allow-other-keys)
  (:documentation "Starts a http server to process remote eval requests."))

(defgeneric bad-request (request &key &allow-other-keys)
  (:documentation "Return http codes and messages to indicate that a request url could not be handled."))

(defgeneric handle-token (server request credentials &key &allow-other-keys)
  (:documentation "Handles a request for a token."))

(defmethod handle-token (server request credentials &key &allow-other-keys)
  (declare (ignorable request))
  (let ((session (start-session server credentials)))
    (token (authorization session))))

(defgeneric handle-eval (server request sexp &key credentials token &allow-other-keys)
  (:documentation "Takes a request from the http server and tanslates it to a call to vergelee-eval."))

(defmethod  handle-eval (server request sexp &key credentials token &allow-other-keys)
  (let* ((*session* (start-session server credentials :token token)))
    (declare (special *session*))
	    (if *session*
		(vergelee-eval sexp)
		(bad-request request))))

(defgeneric vergelee-eval (sexp &key &allow-other-keys)
  (:documentation "Evaluates sexp, by now credentials and tokens should have been checked etc."))

;;TODO: Introduce sanbox. How to expose sandbox setup options.?
(defmethod vergelee-eval (sexp &key &allow-other-keys)
  (let ((result))
      (setf result (if (stringp sexp)
		       (eval (read-from-string sexp))
		       (eval sexp)))
      (format nil "~S" result)))

