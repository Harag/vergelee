(in-package :vergelee)


(defun example ()
  (let ((server (make-instance 'hunchentoot-server :port 4321)))
    (start-server server)))


;;should return bad request
(drakma:http-request
	      "http://localhost:4321/eval"
	      :method :post
	      :parameters (list (cons "sexp" (format nil "~S" '(+ 1 1)))))

;;should return something
(drakma:http-request
	      "http://localhost:4321/token"
	      :method :post
	      :parameters (list (cons "credentials" (format nil "~S" '(:user "piet" :password "password")))))

;;Should return 2
(let ((token (vergelee::get-token "localhost" '(:user "piet" :password "password"))))
  (vergelee::remote-eval "localhost" '(+ 1 1) token))
