(defsystem "vergelee"
  :description "CL remote eval."
  :version "2020.08.22"
  :author "Phil Marneweck"
  :licence "MIT"
  :depends-on ("ironclad"
	       #-:vergelee-alt-server
	       "hunchentoot")
  :components ((:module "vergelee"
			:pathname ""
			:components ((:file "package")	       
				     (:file "server" :depends-on ("package"))))
	       #-:vergelee-alt-server
	       (:module "ver-toot"
			:pathname ""
		;;	:depends-on ("hunchentoot")
			:components ((:file "hunchentoot")))))

